## Prerequisites

- Laravel 5.8
- PHP 7.2

## Instructions

    $ composer require petnetwebapps/user-authentication

Add the provider at **config/app.php**

    'providers' => [
        Petnet\Auth\PetnetServiceProvider::class

Publish the config file at

    php artisan vendor:publish --provider="Petnet\Auth\PetnetServiceProvider"

Delete your user migrations in **database/migrations** directory

Then run the command:

    $ php artisan migrate
    $ php artisan passport:install

In your **app/Http/Middlewares/VerifyCsrfToken.php**, please except the webhook routes by typing

    protected $except = [
        'webhooks/*'
    ];

Register this routes at **routes/web.php**

    Route::webhooks('/webhooks/user', '/webhooks/user');
    Route::webhooks('/webhooks/update-user', '/webhooks/update-user');
    Route::webhooks('/webhooks/delete-user', '/webhooks/delete-user');

Then add the **Petnet\Auth\Concerns\CanVerifyUser.php** in **app/User.php**

Modify the **app/User.php**

    <?php

    namespace App;

    use Petnet\Auth\Models\User as Authenticatable;
    class User extends Authenticatable
    {

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name', 'email', 'password',
        ];
        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'email_verified_at' => 'datetime',
        ];

    }

Now you can access the authentication routes built via hookified

| Route                         | Method | Description                              |
| ----------------------------- | ------ | ---------------------------------------- |
| /api/login                    | POST   | Login the user                           |
| /api/logout                   | POST   | Logout the user                          |
| /api/email/verify/{id}/{hash} | GET    | Get the email and the verify email token |
| /api/password/email           | POST   | Send reset password link                 |
| /api/email/resend             | POST   | Resend email verification                |
| /api/password/reset/{token}   | GET    | Return the forgot password token         |
| /api/password/reset           | POST   | Reset the password                       |
| /api/email/verify             | GET    | Check if account is verified             |

You can seed your dummy users via factories by typing

    $ php artisan tinker 
    >> factory(Petnet\Auth\Models\Role::class, 10)->create();
    >> factory(Petnet\Auth\Models\User::class, 10)->create();


You can also modify the default username of login via **config/user-auth.php**

       'username' => 'employee_id',
