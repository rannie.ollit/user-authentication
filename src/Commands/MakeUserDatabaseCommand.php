<?php

namespace Petnet\Auth\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Symfony\Component\Console\Output\ConsoleOutput;

class MakeUserDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:database',
              $description = 'Creating auth database',
              $consoleOutput;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ConsoleOutput $consoleOutput)
    {
        parent::__construct();
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \DB::statement("CREATE DATABASE " . config('user-auth.auth_database_name', 'auth'));
        } catch (QueryException $th) {
            $this->consoleOutput->writeln("DATABASE " . config('user-auth.auth_database_name', 'auth') . " exists!");
        } catch (\Exception $e) {
            \Log::error('error', ['stacktrace' => $e->getMessage()]);
        }
    }
}
