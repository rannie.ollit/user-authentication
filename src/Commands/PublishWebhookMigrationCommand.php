<?php

namespace Petnet\Auth\Commands;

use Illuminate\Console\Command;

class PublishWebhookMigrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create webhook migration files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Artisan::call('vendor:publish', [
            '--provider' => "Spatie\WebhookClient\WebhookClientServiceProvider", '--tag' => 'migrations'
        ]);
    }
}
