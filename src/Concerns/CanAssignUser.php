<?php

namespace Petnet\Auth\Concerns;

trait CanAssignUser
{
    /**
     *  Get the user instance from another database
     */
    public static function remoteLogin($id)
    {
        $model = new static();
        $model->getConnection()->select(
            \DB::raw("SELECT hris.id AS hris_id, hris.name
        FROM " . config('user-auth.auth_database_name', 'auth') . ".users hris INNER JOIN fis.users fis ON hris.id = fis.id WHERE hris.id = :id"),
            ['id' => $id]
        );
        return $model->firstOrFail();
    }

    /**
     *  Set database connection name for remote user
     */
    protected function getConnectionName()
    {
        return config('user-auth.auth_database_connection', 'mysql2');
    }
}
