<?php

namespace Petnet\Auth\Concerns;

use Petnet\Auth\Notifications\ResetPassword;
use Petnet\Auth\Notifications\VerifyEmail;

trait CanVerifyUser
{
    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
