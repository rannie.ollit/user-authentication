<?php

namespace Petnet\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return config('user-auth.username');
    }

    /**
     *  Return the user
     *  @param  \Illuminate\Http\Request  $request
     */
    public function theUser(Request $request)
    {
        return User::where($this->username(), $request->{$this->username()})->firstOrFail();
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->theUser($request);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function sendLoginResponse(Request $request)
    {
        $user = $this->theUser($request);
        if (\Hash::check($request->password, $user->password)) {
            $this->clearLoginAttempts($request);
            $token = $user->createToken(config('user-auth.token-name'))->accessToken;
            return response()->json(['status' => 'success', 'token' => $token, 'user' => $user->load('roles')]);
        }
        return response()->json(['message' => 'Invalid email address or password!'], 422);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $value = $request->bearerToken();
        if ($value) {
            $id = (new Parser())->parse($value)->getHeader('jti');
            \DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => 1]);
            return Response(['message' => 'You are successfully logged out'], 200);
        }
    }
}
