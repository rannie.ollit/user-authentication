<?php

namespace Petnet\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Petnet\Auth\Services\RoleService;

class RoleAssignmentController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * Assign a user to role
     */
    public function assign(Request $payload)
    {
        return $this->roleService->assignRole($payload->user_id, $payload->role_id, $payload->only('payload'));
    }

    /**
     * Update a user to role
     */

    public function reassign(Request $payload)
    {
        return $this->roleService->updateRole($payload->user_id, $payload->role_id, $payload->only('payload'));
    }


    /**
     * Update a user to role
     */

    public function fetchRoles(int $id)
    {
        return $this->roleService->getUserDetails($id);
    }


    /**
     * Update a user to role
     */

    public function deleteRoles(int $id)
    {
        return $this->roleService->deleteUserRoles($id);
    }
}
