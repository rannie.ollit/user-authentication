<?php

namespace Petnet\Auth\Jobs;

use App\User;
use Spatie\WebhookClient\ProcessWebhookJob as BaseJob;

class ProcessDeleteUserJob extends BaseJob
{
    public $tries = 3;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        User::findOrFail($this->webhookCall->payload['user']['id'])->delete();
        \Log::debug($this->webhookCall);
    }
}
