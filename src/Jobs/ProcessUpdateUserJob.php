<?php

namespace Petnet\Auth\Jobs;

use App\User;
use Spatie\WebhookClient\ProcessWebhookJob as BaseJob;

class ProcessUpdateUserJob extends BaseJob
{
    public $tries = 3;
    
    public function handle()
    {
        User::findOrFail($this->webhookCall->payload['user']['id'])->update($this->webhookCall->payload['user']);
        \Log::debug($this->webhookCall);
    }
}
