<?php

namespace Petnet\Auth\Jobs;

use App\User;
use Spatie\WebhookClient\ProcessWebhookJob as BaseJob;

class ProcessWebhookJob extends BaseJob
{
    public $tries = 3;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        User::create($this->webhookCall->payload['user']);
        \Log::debug($this->webhookCall);
    }
}
