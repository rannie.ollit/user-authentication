<?php

namespace Petnet\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'],
        $guarded = ['id'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_role')
            ->using(UserRole::class)
            ->withPivot([
                'payload'
            ]);
    }
}
