<?php

namespace Petnet\Auth\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Petnet\Auth\Concerns\CanVerifyUser;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens, CanVerifyUser, SoftDeletes;

    protected $dates = ['deleted_at'],
        $guarded = ['id'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role')
        ->using(UserRole::class)
        ->withPivot([
            'payload'
        ]);
    }
}
