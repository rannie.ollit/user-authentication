<?php

namespace Petnet\Auth\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
    protected $table = 'user_role',
        $cast = [
            'payload' => 'array',
        ];
}
