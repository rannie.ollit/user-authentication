<?php

namespace Petnet\Auth;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Petnet\Auth\Commands\MakeUserDatabaseCommand;
use Petnet\Auth\Commands\PublishWebhookMigrationCommand;

class PetnetServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerEloquentFactoriesFrom(__DIR__ . '/Factories');

        $this->mergeConfigFrom(
            __DIR__ . '/user-auth.php', 'user-auth'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/webhook-client.php', 'webhook-client'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeUserDatabaseCommand::class,
                PublishWebhookMigrationCommand::class
            ]);
        }

        $this->loadMigrationsFrom(__DIR__ . '/migrations');


        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->publishes([
            __DIR__ . '\user-auth.php' => config_path('user-auth.php'),
            __DIR__ . '\webhook-client.php' => config_path('webhook-client.php'),
        ], 'config');

        $this->loadAuthRoutes();
    }

    /**
     * Register model factories
     */
    protected function registerEloquentFactoriesFrom($path)
    {
        $this->app->make(Factory::class)->load($path);
    }

    /**
     * Load auth routes
     */
    public function loadAuthRoutes()
    {

        Route::namespace('Petnet\Auth\Http\Controllers')
            ->middleware('api')
            ->prefix('api')
            ->group(function () {
                Route::post('/logout', 'LoginController@logout')->name('logout');
                Route::post('/login', 'LoginController@login')->name('login');
                Route::get('/email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');
                Route::get('/email/verify', 'VerificationController@show')->name('verification.notice');
                Route::post('/email/resend', 'VerificationController@resend')->name('verification.resend');
                Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
                Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
                Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');
            });
        # code...
    }
}
