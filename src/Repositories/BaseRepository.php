<?php

namespace Petnet\Auth\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


abstract class BaseRepository
{
    public abstract function add(array $data): Model;

    public abstract function update(array $data, int $id): Model;

    public abstract function get(int $id): Model;

    public abstract function all(array $data = ['*']): Collection;

    public abstract function delete(int $id): Collection;
}
