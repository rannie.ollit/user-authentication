<?php

namespace Petnet\Auth\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Petnet\Auth\Models\Role;

class RoleRepository extends BaseRepository
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function add(array $data): Model
    {
        return $this->role->create($data);
    }

    public function update(array $data, int $id): Model
    {
        return tap($this->get($id))->update($data);
    }

    public function get(int $id): Model
    {
        return $this->role->findOrFail($id);
    }

    public function all(array $data = ['*']): Collection
    {
        return $this->role->all($data);
    }

    public function delete(int $id): Collection
    {
        $this->get($id)->delete();
        return $this->all();
    }
}
