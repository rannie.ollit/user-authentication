<?php

namespace Petnet\Auth\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Petnet\Auth\Models\User;

class RoleService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Assign role to a user
     */
    public function assignRole(int $userId, int $roleId, $data): JsonResponse
    {
        $this->user->findOrFail($userId)->roles()->attach($roleId, ['payload' => json_encode($data['payload'])]);
        return response()->json(['message' => 'user role given!']);
    }

    /**
     * Update role to a user
     */
    public function updateRole(int $userId, int $roleId, $data): JsonResponse
    {
        if ($user = $this->user->findOrFail($userId)) {
            $user->roles()->sync([$roleId => ['payload' => json_encode($data['payload'])]]);
            return response()->json(['message' => 'user role updated!']);
        }
    }

    /**
     * get user details with roles
     */

     public function getUserDetails(int $userId): Collection
     {
         return $this->user->findOrFail($userId)->roles;
     }

    /**
     * Delete user roles
     */

    public function deleteUserRoles(int $userId): JsonResponse
    {
        $this->user->findOrFail($userId)->roles()->detach();
        return response()->json(['message' => 'roles deleted!']);
    }
}
