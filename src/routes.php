<?php
//creating roles
Route::apiResource('/api/roles', 'Petnet\Auth\Http\Controllers\RolesController');
//role assignment
Route::post('/api/assign-roles', 'Petnet\Auth\Http\Controllers\RoleAssignmentController@assign');
Route::put('/api/update-roles', 'Petnet\Auth\Http\Controllers\RoleAssignmentController@reassign');
Route::get('/api/get-roles/{id}', 'Petnet\Auth\Http\Controllers\RoleAssignmentController@fetchRoles');
Route::delete('/api/delete-roles/{id}', 'Petnet\Auth\Http\Controllers\RoleAssignmentController@deleteRoles');
