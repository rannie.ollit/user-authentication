<?php

return [
    'auth_database_name' => env('AUTH_DATABASE_NAME', "auth"),

    'auth_database_connection' => env('AUTH_DATABASE_CONNECTION', 'mysql2'),

    'webhook_client_secret' => env('WEBHOOK_CLIENT_SECRET', 'sign-using-this-secret'),

    'username' => 'employee_id',

    'token_name' => 'test'
];
