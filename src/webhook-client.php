<?php

return [
    'configs' => [
        [
            'name' => '/webhooks/user',
            'signing_secret' => config('user-auth.webhook_client_secret'),
            'signature_header_name' => 'Signature',
            'signature_validator' => \Spatie\WebhookClient\SignatureValidator\DefaultSignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'process_webhook_job' => \Petnet\Auth\Jobs\ProcessWebhookJob::class
        ],
        [
            'name' => '/webhooks/update-user',
            'signing_secret' => config('user-auth.webhook_client_secret'),
            'signature_header_name' => 'Signature',
            'signature_validator' => \Spatie\WebhookClient\SignatureValidator\DefaultSignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'process_webhook_job' => \Petnet\Auth\Jobs\ProcessUpdateUserJob::class
        ],
        [
            'name' => '/webhooks/delete-user',
            'signing_secret' => config('user-auth.webhook_client_secret'),
            'signature_header_name' => 'Signature',
            'signature_validator' => \Spatie\WebhookClient\SignatureValidator\DefaultSignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'process_webhook_job' => \Petnet\Auth\Jobs\ProcessDeleteUserJob::class
        ]
    ],
];
